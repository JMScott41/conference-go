from .keys import OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # print("get_photo")
    r = requests.get(f"https://api.pexels.com/v1/search?query={city}{state}&per_page=1", headers={'Authorization': "g7OpQo451qppdhzNOANZ3Dbc9l0NXU2vlGn0rDHOnkIPmK730LfIX8ax"})
    # print(r.json())
    content = json.loads(r.content)
    print(content["photos"])
    return {
        "picture_url": content["photos"][0]["src"]["original"]
     }
# get_photo("bronx", "NY")


def get_weather_data(city, state):
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit={1}&appid={OPEN_WEATHER_API_KEY}")
    geocode = response.json()
    lat = geocode[0]["lat"]
    lon = geocode[0]["lon"]

    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}")
    weather_info = response.json()
    description = {
        "main_temperature": weather_info["main"]["temp"],
        "weather_description": weather_info["weather"][0]["description"],
    }

    return description
